package main

import (
	"bufio"
	"bytes"
	"context"
	"flag"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/tklauser/go-sysconf"
)

var (
	addr           = flag.String("addr", ":3909", "address to listen on")
	updateInterval = flag.Duration("interval", 10*time.Second, "update interval")
	doDebug        = flag.Bool("debug", false, "log debug messages")

	userHZ float64
)

func init() {
	userHZ = 100
	if clktck, err := sysconf.Sysconf(sysconf.SC_CLK_TCK); err == nil {
		userHZ = float64(clktck)
	}
}

func debug(s string, args ...interface{}) {
	if *doDebug {
		log.Printf(s, args...)
	}
}

func splitServiceName(path string) (string, string) {
	slice, name := filepath.Split(path)
	slice = strings.Trim(slice, "/")
	return slice, name
}

func parseMapFile(path string) (map[string]int64, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	result := make(map[string]int64)
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Bytes()
		parts := bytes.Split(line, []byte(" "))
		if len(parts) != 2 {
			continue
		}
		value, err := strconv.ParseInt(string(parts[1]), 10, 64)
		if err != nil {
			continue
		}
		result[string(parts[0])] = value
	}
	return result, scanner.Err()
}

func parseBlkioMapFile(path string) (map[string]int64, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	// Aggregate counts by operation type (sum by device).
	result := make(map[string]int64)
	scanner := bufio.NewScanner(f)
	for scanner.Scan() {
		line := scanner.Bytes()
		parts := bytes.Split(line, []byte(" "))
		if len(parts) != 3 {
			continue
		}
		value, err := strconv.ParseInt(string(parts[2]), 10, 64)
		if err != nil {
			continue
		}
		result[string(parts[1])] += value
	}

	return result, scanner.Err()
}

// func parseSingleValueFile(path string) (int64, error) {
// 	data, err := ioutil.ReadFile(path)
// 	if err != nil {
// 		return 0, err
// 	}
// 	return strconv.ParseInt(string(data), 10, 64)
// }

func cgroupStatPath(cgroupPath, collector, path string) string {
	return filepath.Join("/sys/fs/cgroup", collector, cgroupPath, path)
}

type cpuParser struct {
	desc *prometheus.Desc
}

func newCPUParser() *cpuParser {
	return &cpuParser{
		desc: prometheus.NewDesc(
			"cgroup_cpu_usage",
			"Cgroup CPU usage.",
			[]string{"mode", "slice", "service"},
			nil,
		),
	}
}

func (p *cpuParser) describe(ch chan<- *prometheus.Desc) {
	ch <- p.desc
}

func (p *cpuParser) parse(path string) ([]prometheus.Metric, error) {
	usage, err := parseMapFile(cgroupStatPath(path, "cpu,cpuacct", "cpuacct.stat"))
	if err != nil {
		return nil, err
	}

	slice, name := splitServiceName(path)

	return []prometheus.Metric{
		prometheus.MustNewConstMetric(
			p.desc,
			prometheus.GaugeValue,
			float64(usage["user"])/userHZ,
			"user", slice, name,
		),
		prometheus.MustNewConstMetric(
			p.desc,
			prometheus.GaugeValue,
			float64(usage["system"])/userHZ,
			"system", slice, name,
		),
	}, nil
}

type memoryParser struct {
	desc *prometheus.Desc
}

func newMemoryParser() *memoryParser {
	return &memoryParser{
		desc: prometheus.NewDesc(
			"cgroup_memory_usage",
			"Cgroup memory usage (RSS, in bytes).",
			[]string{"slice", "service"},
			nil,
		),
	}
}

func (p *memoryParser) describe(ch chan<- *prometheus.Desc) {
	ch <- p.desc
}

func (p *memoryParser) parse(path string) ([]prometheus.Metric, error) {
	mstat, err := parseMapFile(cgroupStatPath(path, "memory", "memory.stat"))
	if err != nil {
		return nil, err
	}

	slice, name := splitServiceName(path)

	return []prometheus.Metric{
		prometheus.MustNewConstMetric(
			p.desc,
			prometheus.GaugeValue,
			float64(mstat["total_rss"]),
			slice, name,
		),
	}, nil
}

type blkioParser struct {
	bytesDesc, latencyDesc *prometheus.Desc
}

func newBlkioParser() *blkioParser {
	return &blkioParser{
		bytesDesc: prometheus.NewDesc(
			"cgroup_blkio_bytes",
			"Bytes read/written by blkio.",
			[]string{"mode", "slice", "service"},
			nil,
		),
		latencyDesc: prometheus.NewDesc(
			"cgroup_blkio_latency_ns",
			"Average blkio operation latency (in nanoseconds).",
			[]string{"mode", "slice", "service"},
			nil,
		),
	}
}

func (p *blkioParser) describe(ch chan<- *prometheus.Desc) {
	ch <- p.bytesDesc
	ch <- p.latencyDesc
}

func (p *blkioParser) parse(path string) ([]prometheus.Metric, error) {
	ops, err := parseBlkioMapFile(cgroupStatPath(path, "blkio", "blkio.io_serviced"))
	if err != nil {
		return nil, err
	}
	times, err := parseBlkioMapFile(cgroupStatPath(path, "blkio", "blkio.io_service_time"))
	if err != nil {
		return nil, err
	}
	totBytes, err := parseBlkioMapFile(cgroupStatPath(path, "blkio", "blkio.io_service_bytes"))
	if err != nil {
		return nil, err
	}

	slice, name := splitServiceName(path)

	m := []prometheus.Metric{
		prometheus.MustNewConstMetric(
			p.bytesDesc,
			prometheus.CounterValue,
			float64(totBytes["Write"]),
			"write", slice, name,
		),
		prometheus.MustNewConstMetric(
			p.bytesDesc,
			prometheus.CounterValue,
			float64(totBytes["Read"]),
			"read", slice, name,
		),
	}

	// This is unfortunately an average.
	if ops["Write"] > 0 {
		m = append(m, prometheus.MustNewConstMetric(
			p.latencyDesc,
			prometheus.GaugeValue,
			float64(times["Write"])/float64(ops["Write"]),
			"write", slice, name,
		))
	}
	if ops["Read"] > 0 {
		m = append(m, prometheus.MustNewConstMetric(
			p.latencyDesc,
			prometheus.GaugeValue,
			float64(times["Read"])/float64(ops["Read"]),
			"read", slice, name,
		))
	}

	return m, nil
}

type subsystem interface {
	parse(string) ([]prometheus.Metric, error)
	describe(chan<- *prometheus.Desc)
}

var subsystems = []subsystem{
	newCPUParser(),
	newMemoryParser(),
	newBlkioParser(),
}

func walkCGroups() ([]prometheus.Metric, error) {
	rootDir := "/sys/fs/cgroup/systemd"
	var metrics []prometheus.Metric
	err := filepath.Walk(rootDir, func(path string, info os.FileInfo, err error) error {
		if err != nil || !info.IsDir() || !strings.HasSuffix(path, ".service") {
			return nil
		}
		// Do not track systemd internal services.
		if strings.HasPrefix(info.Name(), "systemd-") {
			return nil
		}
		m, err := walkCGroup(path[len(rootDir)+1:])
		if err != nil {
			return nil
		}
		metrics = append(metrics, m...)
		return nil
	})
	return metrics, err
}

func walkCGroup(path string) ([]prometheus.Metric, error) {
	debug("found service %s", path)

	var metrics []prometheus.Metric
	for _, s := range subsystems {
		m, err := s.parse(path)
		if err != nil {
			debug("service %s, subsystem %v: error: %v", path, s, err)
			continue
		}
		metrics = append(metrics, m...)
	}

	return metrics, nil
}

// Keep a pre-rendered snapshot of all the metrics, so that scraping
// and updates can be independent of each other (but still serve a
// coherent view of all the metrics).
type collector struct {
	mx      sync.Mutex
	metrics []prometheus.Metric
	readyCh chan bool
}

func newCollector() *collector {
	return &collector{
		readyCh: make(chan bool),
	}
}

func (c *collector) WaitReady() {
	<-c.readyCh
}

func (c *collector) Describe(ch chan<- *prometheus.Desc) {
	for _, s := range subsystems {
		s.describe(ch)
	}
}

func (c *collector) Collect(ch chan<- prometheus.Metric) {
	c.mx.Lock()
	count := 0
	for _, m := range c.metrics {
		ch <- m
		count++
	}
	debug("collected %d metrics", count)
	c.mx.Unlock()
}

func (c *collector) update(metrics []prometheus.Metric) {
	c.mx.Lock()
	c.metrics = metrics
	c.mx.Unlock()
}

func (c *collector) loop(ctx context.Context) {
	if m, err := walkCGroups(); err == nil {
		c.update(m)
	}
	close(c.readyCh)

	ticker := time.NewTicker(*updateInterval)
	defer ticker.Stop()
	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			if m, err := walkCGroups(); err == nil {
				c.update(m)
			}
		}
	}
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	c := newCollector()
	reg := prometheus.NewRegistry()
	reg.MustRegister(c)

	// Create a cancelable Context and cancel it when we receive a
	// termination signal. This will stop the metrics updater.
	ctx, cancel := context.WithCancel(context.Background())

	// Run the update loop in a goroutine.
	go c.loop(ctx)

	// Only start the HTTP server if we have collected the first
	// round of metrics.
	c.WaitReady()

	// Create a very simple HTTP server that only exposes the
	// Prometheus metrics handler.
	mux := http.NewServeMux()
	mux.Handle("/metrics", promhttp.HandlerFor(reg, promhttp.HandlerOpts{}))
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path == "/" {
			io.WriteString(w, `<html>
<body><h1>cgroups-exporter</h1><p><a href=\"/metrics\">/metrics</a></p>
</body>`)
		} else {
			http.NotFound(w, r)
		}
	})

	// Set up the Listener separately to work around Go 1.7's lack
	// of the http.Server.Close() function.
	l, err := net.Listen("tcp", *addr)
	if err != nil {
		log.Fatal(err)
	}

	srv := &http.Server{
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   20 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		cancel()
		l.Close()
	}()
	signal.Notify(sigCh, syscall.SIGTERM, syscall.SIGINT)

	log.Fatal(srv.Serve(l))
}
