
cgroups-exporter
================

A simple standalone daemon that reads cgroup resource accounting data
and exports it to Prometheus.

